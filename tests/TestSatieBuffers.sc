TestSatieBuffers : SatieUnitTest {

	var server, satie, config;
	var a11wlk01;

	setUp {
		a11wlk01 = Platform.resourceDir +/+ "sounds/a11wlk01.wav";
		server = Server(this.class.name);
		config = SatieConfiguration(server);
		satie = Satie(config);

		this.boot(satie);
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_load_lebedev50buffer {
		this.assert(
			satie.audioSamples[\lebedev50].notNil,
			"audioSamples should contain SatieFactory.lebedev50Buffer after boot"
		);
	}

	test_loadSample {
		var name = \mySample;
		satie.loadSample(name, a11wlk01);
		1.0.wait;
		server.sync;

		this.assertEquals(
			satie.audioSamples[name].class,
			Buffer,
			"audioSamples dictionary should contain a Buffer stored under its name"
		);
	}

	test_loadSample_standardizePath {
		var name = \mySample;
		var path = "~/audio.aiff";
		var sndFile = SoundFile.openWrite(path.standardizePath);
		var data = FloatArray.fill(512, { 1.0.rand2 });
		sndFile.writeData(data);
		sndFile.close;

		// loadSample uses standardizePath internally
		satie.loadSample(name, path);
		server.sync;

		this.assertEquals(
			satie.audioSamples[name].class,
			Buffer,
			"audioSamples dictionary should contain a Buffer stored under its name"
		);
	}

	test_name_not_symbol {
		var size = satie.audioSamples.size;
		satie.loadSample("foobar", a11wlk01);
		this.assertEquals(
			satie.audioSamples.size,
			size,
			"loadSample should not allow non-Symbol names"
		);
	}

}

