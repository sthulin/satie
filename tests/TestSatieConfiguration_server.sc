TestSatieConfiguration_server : SatieUnitTest {

	var satie, server;

	setUp {
		server = Server(this.class.name);
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_listeningFormat_numChannels {
		var spat = \domeVBAP;
		var config = SatieConfiguration(server, [spat]);
		satie = Satie(config);
		this.boot(satie);

		this.assertEquals(
			server.options.numOutputBusChannels,
			satie.config.spatializers[spat].numChannels,
			"Server has correct number of output channels"
		);
	}

	test_outBusIndex_numChannels {
		var outOffsets = [2, 4];
		var spat = \stereoListener;
		var config = SatieConfiguration(server, [spat, spat], outBusIndex: outOffsets);
		satie = Satie(config);
		this.boot(satie);

		this.assertEquals(
			server.options.numOutputBusChannels,
			6,
			"Server output channels correctly offset"
		);
	}
}
