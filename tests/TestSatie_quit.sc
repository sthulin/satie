TestSatie_quit : SatieUnitTest {

	var satie, server;

	setUp {
		server = Server(this.class.name);
		satie = Satie(SatieConfiguration(server));

		satie.makeProcess(\dummy, ( ));
		satie.makeProcessInstance(\testProcess, \dummy);

		// Boot Satie, wait, then quit Satie
		this.boot(satie);
		0.2.wait;
		this.quit(satie);
	}

	tearDown {
		// satie already quit
		server.remove;
	}

	test_booted_false {
		this.assertEquals(
			satie.booted,
			false,
			"Booted is false after quit"
		);
	}

	test_serverRunning_false {
		this.assertEquals(
			server.serverRunning,
			false,
			"Server not running after quit"
		);
	}

	test_groups_free {
		this.assertEquals(
			satie.groups.isEmpty,
			true,
			"Groups where freed after quit"
		);
	}

	test_groupInstances_free {
		this.assertEquals(
			satie.groupInstances.isEmpty,
			true,
			"Group Instances where freed after quit"
		);
	}

	test_processInstance_free {
		this.assertEquals(
			satie.processInstances.isEmpty,
			true,
			"Process Instances where freed after quit"
		);
	}

	test_oscDefs_base_remain {
		var base = [\satieConfigure, \satieBoot, \satieReboot, \satieQuit, \satieHeartbeat];
		var keys = satie.osc.oscDefs.keys;

		this.assertEquals(
			keys.size,
			5,
			"Satie should only have 5 OSC handlers remaining after quit"
		);

		keys.do { |key|
			this.assert(
				base.includes(key),
				"Satie base OSC handler % was still active after quit".format(key)
			);
		};
	}

	test_cmdPeriod_remove {
		this.assertEquals(
			CmdPeriod.objects.includes(satie),
			false,
			"Satie removed from CmdPeriod"
		);
	}

	test_serverTree_remove {
		this.assertEquals(
			ServerTree.objects.at(server).includes(satie),
			false,
			"Satie removed from ServerTree"
		);
	}

	test_oscDisconnected {
		this.assertEquals(
			satie.osc.oscServer.isConnected,
			false,
			"OSC server is disconnected"
		);
	}

	test_audioSamples_clear {
		this.assertEquals(
			satie.audioSamples.isEmpty,
			true,
			"Satie cleared its audioSamples dictionary after quit"
		)
	}

	test_status_initialized {
		this.assertEquals(
			satie.status,
			\initialized,
			"Satie's status should return to initialized after quit"
		);
	}

	test_satieOSC_heartbeat_persists {
		// Satie heartbeat should be disabled by default
		this.assertEquals(
			satie.osc.heartbeat,
			false,
			"SatieOSC heartbeat should be false by default"
		);

		satie.osc.heartbeat = true;
		this.assertEquals(
			satie.osc.heartbeat,
			true,
			"SatieOSC heartbeat should have been set to true"
		);

		// Boot Satie, wait, then quit Satie
		this.boot(satie);
		0.2.wait;
		this.quit(satie);

		this.assertEquals(
			satie.osc.heartbeat,
			true,
			"SatieOSC heartbeat should have persisted after Satie status returned to \initialized"
		);
	}
}
