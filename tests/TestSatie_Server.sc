TestSatie_Server : SatieUnitTest {

	var server, satie;

	setUp {
		server = Server(this.class.name);
		satie = Satie(SatieConfiguration(server));
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_boot {
		this.boot(satie);

		this.assertEquals(satie.status, \running, "Satie.boot should have booted the server");
	}

	test_waitForBoot {
		var timeout, cond = Condition.new;
		var foo = false;

		timeout = fork { 10.wait; cond.unhang; "% Satie failed to boot".format(thisMethod).warn };
		satie.waitForBoot({
			this.assertEquals(satie.status, \running, "Status should be \\running when waitForBoot function is evaluated");
			foo = true;
			cond.unhang;
		});
		cond.hang;
		timeout.stop;

		this.assertEquals(foo, true, "Satie.waitForBoot should have executed its onComplete function");
	}

	test_waitForBoot_when_booting {
		var timeout, cond = Condition.new;
		var foo = false;

		timeout = fork { 10.wait; cond.unhang; "% Satie failed to boot".format(thisMethod).warn };
		satie.boot;
		satie.waitForBoot({
			this.assertEquals(satie.status, \running, "Status should be \\running when onComplete function is evaluated");
			foo = true;
			cond.unhang;
		});
		cond.hang;
		timeout.stop;

		this.assertEquals(foo, true, "Satie.waitForBoot should have evaluated onComplete once status was \\running");
	}

	test_waitForBoot_when_running {
		var timeout, cond = Condition.new;
		var foo = false;
		var limit = 50;

		satie.boot;
		while { (satie.status !== \running) && (limit > 0) } {
			0.2.wait;
			limit = limit - 1;
		};
		if(satie.status !== \running) {
			this.failed(thisMethod.name, "% Satie did not boot correctly".format(thisMethod.name));
		} {
			timeout = fork { 10.wait; cond.unhang; "% Satie failed to boot".format(thisMethod).warn };
			satie.waitForBoot({
				foo = true;
				cond.unhang;
			});
			cond.hang;
			timeout.stop;

			this.assertEquals(foo, true, "Satie.waitForBoot should have evaluated onComplete once status was \\running");
		}
	}

	test_reboot_when_initialized {
		var limit = 50;

		this.assertNoException(
			{ satie.reboot },
			message: "Satie.reboot should not have thrown error when status was \\initialized"
		);

		while { (satie.status === \initialized) && (limit > 0) } {
			0.2.wait;
			limit = limit - 1;
		};
		this.assertEquals(satie.status, \booting, "Satie.reboot called boot when status was \\initialized");

		this.wait({ satie.status === \running }, "% Satie failed to boot".format(thisMethod), 30);
	}

	test_reboot_when_running {
		var limit = 50;

		this.boot(satie);

		this.assertNoException(
			{ satie.reboot },
			message: "Satie.reboot should not have thrown error when status was \\running"
		);

		while { (satie.status !== \booting) && (limit > 0) } {
			0.2.wait;
			limit = limit - 1;
		};
		this.assertEquals(satie.status, \booting, "Satie.reboot rebooted Satie when status was \\running");

		this.wait({ satie.status === \running }, "% Satie failed to boot".format(thisMethod), 30);
	}

	test_create_groups {
		this.boot(satie);

		this.assertEquals(satie.groups.keys, Set[\default, \defaultFx, \postProc, \ambiPostProc], "Satie should have created its groups after boot");
	}

	test_group_ordering {
		var reply;
		var groupIDs;
		var groupOrder = [\default, \defaultFx, \ambiPostProc, \postProc];
		var osc = OSCFunc({ |msg| reply = msg }, '/g_queryTree.reply', server.addr).oneShot;
		this.boot(satie);

		server.sendMsg('/g_queryTree', server.defaultGroup.nodeID);
		server.sync;

		// get the NodeIDs from the query reply message
		groupIDs = [4, 6, 8, 10].collect { |i| reply[i] };
		satie.groups.size.do { |i|
			this.assertEquals(
				groupIDs[i],
				satie.groups.at(groupOrder[i]).nodeID,
				"Satie should have created its groups in the correct order"
			);
		};
	}

	test_reconfigure_when_running {
		var limit = 50;
		var outbus = [4];

		this.boot(satie);

		this.assertNoException(
			{ satie.reconfigure(SatieConfiguration(server: server, outBusIndex: outbus)); },
			message: "Satie.reconfigure should not have thrown error when status was \\running"
		);

		while { (satie.status !== \booting) && (limit > 0) } {
			0.2.wait;
			limit = limit - 1;
		};
		this.assertEquals(satie.status, \booting, "Satie.reconfigure should have called reboot");

		while { (satie.status !== \running) && (limit > 0) } {
			0.2.wait;
			limit = limit - 1;
		};
		this.assertEquals(satie.status, \running, "Satie's status should be \\running after reboot");

		this.assertEquals(
			satie.config.outBusIndex,
			outbus,
			"Satie.reconfigure should have reconfigured and rebooted Satie when status was \\running"
		);
	}
}
