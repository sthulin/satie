TestSatieAmbiPostProc : SatieUnitTest {

	var server, satie, config;

	setUp {
		server = Server(this.class.name);
		config = SatieConfiguration(server, ambiOrders: [1, 3]);
		config.generateSynthdefs = false;
		satie = Satie(config);
		this.boot(satie);
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_replaceAmbiPostProcessor_order_exception {
		this.assertException(
			{ satie.replaceAmbiPostProcessor(\Identity, order: 2) },
			Error,
			"Only configured ambisonic orders should be allowed by replaceAmbiPostProc"
		)
	}

}

