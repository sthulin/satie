TestSatieStatusWatcher : SatieUnitTest {

	var watcher;

	setUp {
		watcher = SatieStatusWatcher(\mock_satie, \mock_server);
	}

	tearDown {
		watcher = nil;
	}

	test_init_status {
		this.assertEquals(
			watcher.status,
			\initialized,
			"SatieStatusWatcher status should be \initialized after instantiation"
		);
	}

	test_init_booted {
		this.assertEquals(
			watcher.booted,
			false,
			"SatieStatusWatcher booted should be false after instantiation"
		);
	}

	test_postServerBoot_returns_function {
		this.assertEquals(
			watcher.postServerBoot.class,
			Function,
			"SatieStatusWatcher's postServerBoot method should return a function"
		);
	}

	test_satieBoot_status_not_initialized {
		watcher.status = \foobar;
		this.assertException(
			{ watcher.satieBoot },
			Error,
			"satieBoot should throw an error if status isn't \\initialized"
		);
	}

	test_waitForSatieRunning_when_status_invalid {
		watcher.status = \foobar;
		this.assertException(
			{ watcher.waitForSatieRunning },
			Error,
			"waitForSatieRunning should throw error when status is invalid"
		);
	}

	test_doWhenSatieRunning_timeout_failure {
		var timeout, cond = Condition.new;
		var result = false;

		// fake status won't change
		watcher.status = \booting;
		// this should time out and post an error message
		watcher.doWhenSatieRunning({ result = true });
		1.wait;
		"This test will throw an error. It should be ignored.".warn;
		"takes 10 seconds to finish...".warn;
		timeout = fork { 10.wait; cond.unhang };
		cond.hang;

		this.assertEquals(
			result,
			false,
			"doWhenSatieRunning should not evaluate onComplete when timed out"
		);
	}

	test_doWhenSatieRunning_status_failure {
		var result = false;

		// invalid status
		watcher.status = \initialized;
		// this should post an error message immediately
		this.assertException(
			{ watcher.doWhenSatieRunning },
			Error,
			"doWhenSatieRunning should throw an error when status is invalid"
		);
	}

}
