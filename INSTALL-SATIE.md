## Getting Started with SATIE on Ubuntu and macOS

This document will help get you started with SATIE by guiding you through its installation on _Ubuntu 20.04+_ and _macOS_.
It's recommended that you use SATIE with the latest stable versions of SuperCollider and sc3-plugins. For Higher-order Ambisonics, SATIE makes use of the _HOAUGens_ (available in sc3-plugins 3.10+).
SATIE is designed to work with SuperCollider's alternative multithreaded audio server _supernova_, as well as _scsynth_.

In order to use SATIE, you will need:
* git (https://git-scm.com/download/)
* SuperCollider 3.11+
* sc3-plugins 3.11+
* SATIE quark
* MathLib and SC-HOA quarks (dependencies pulled by SATIE)

In-depth documentation and platform specific installation instructions for SuperCollider and sc3-plugins can be found in their respective Github repositories:
* [SuperCollider on GitHub](https://github.com/supercollider/supercollider/tree/main)
* [sc3-plugins on GitHub](https://github.com/supercollider/sc3-plugins/tree/main)

## macOS

#### Installing SuperCollider

To install SuperCollider, head over to the releases page and download the macOS release binary:

https://github.com/supercollider/supercollider/releases

Extract and drag `SuperCollider` into your `Applications` folder.

#### Installing sc3-plugins

To install the sc3-plugins, head over to the releases page and download the macOS release binary:

https://github.com/supercollider/sc3-plugins/releases

> **Note:** Be sure to download the version of sc3-plugins that most closely matches your version of SuperCollider

Extract and place the sc3-plugins folder in your SuperCollider extensions folder. By default, this folder is located at:

```
~/Library/Application Support/SuperCollider/Extensions
```



## Ubuntu
### Installing pre-built binaries

We maintain Ubuntu packages on our [Launchpad PPA](https://launchpad.net/~sat-metalab/+archive/ubuntu/metalab).
For easy install on a Debian based system (tested only on Ubuntu):

```
sudo add-apt-repository ppa:sat-metalab/metalab
sudo apt-get update
```
Then:

`sudo apt install supercollider sc3-plugins`

Then skip to [Install SATIE](#install-satie) for instructions to complete the installation.

### Compiling by hand
#### Installing SuperCollider

Recent SuperCollider versions aren't packaged for Ubuntu at the time of writing. Building it from source is therefore required.
The following instructions will guide you through the build process on Ubuntu 20.04 and up.
If you are using a prior version of Ubuntu, follow the instructions provided in SuperCollider's
[Linux README](https://github.com/supercollider/supercollider/blob/main/README_LINUX.md).

##### Install build dependencies

Before building SuperCollider, you must install its build dependencies. The required packages are available to install using Ubuntu's package manager.

Use this command to install the packages:

```
sudo apt-get install build-essential libsndfile1-dev libasound2-dev libjack-jackd2-dev \
libavahi-client-dev libicu-dev libreadline-dev libncurses-dev libfftw3-dev libxt-dev libudev-dev pkg-config git cmake \
qt5-default qt5-qmake qttools5-dev qttools5-dev-tools qtdeclarative5-dev qtpositioning5-dev \
libqt5sensors5-dev libqt5opengl5-dev qtwebengine5-dev libqt5svg5-dev libqt5websockets5-dev
```

##### Clone SuperCollider

You will need to clone the SuperCollider Github repository and fetch the project's submodules.

Assuming you want to clone the repository into a folder called `~/src`, run the following commands:

```
cd ~/src
git clone --branch main --recurse-submodules https://github.com/supercollider/supercollider.git
cd supercollider
```

##### Build and install

Create a build folder, then configure, make, and install:

```
cd ~/src/supercollider
mkdir build && cd build
```
A default Ubuntu will not install the Emacs editor. If you don't know what Emacs is or have no intention of using it with SuperCollider, you will want to disable support for it:
```
cmake -DCMAKE_BUILD_TYPE=Release -DNATIVE=ON -DSC_EL=OFF ..   # turn off emacs-based IDE
```
However, if your OS has Emacs installed, you may simply do
```
cmake -DCMAKE_BUILD_TYPE=Release -DNATIVE=ON ..
```

```
make
sudo make install
sudo ldconfig    # needed when building SuperCollider for the first time
```
#### Installing sc3-plugins

Like SuperCollider, recent sc3-plugins versions aren't currently packaged for Ubuntu. Building them from source is therefore required.
You will need to clone the sc3-plugins Github repository and fetch the project's submodules.

Assuming you want to clone the repository into a folder called `~/src`, run the following commands:

```
cd ~/src
git clone --branch main --recurse-submodules https://github.com/supercollider/sc3-plugins.git
cd sc3-plugins
```

Create a build folder, then configure, build, and install:

```
mkdir build && cd build
cmake -DSC_PATH=../../supercollider/ -DSUPERNOVA=ON -DCMAKE_BUILD_TYPE=Release -DNATIVE=ON ..
make
sudo make install
```

## Raspeberry Pi

Install SuperCollider and sc3-plugins following those instructions:
https://github.com/supercollider/supercollider/blob/develop/README_RASPBERRY_PI.md

Then go to [Install SATIE](#install-satie) for instructions to complete the installation.

## Windows

### Installing Supercollider

SuperCollider maintainers provide detailed [installation instructions for Windows](https://github.com/supercollider/supercollider/blob/develop/README_WINDOWS.md#installing-supercollider).

### Installing sc3-plugins

In order to install [sc3-plugins](https://github.com/supercollider/sc3-plugins), follow the official [installation guide](https://github.com/supercollider/sc3-plugins#installation)

### Jack Audio Connection Kit

We also recommend installing [JACK](https://jackaudio.org). It allows routing audio signals from arbitrary audio applications to SATIE for spatialization. Downloadable installers are [available](https://jackaudio.org/downloads/) for major operating systems.

## Install SATIE

The SATIE quark can be installed directly from within the SuperCollider IDE (scide).

In a new document, write the following line of code and evaluate it by hitting `Ctrl-Enter`:

```
Quarks.install("SATIE");
```

This will fetch SATIE quark through official supercollider repo.

If you want to use a local checkout of SATIE code (for developpement purpose), you can use this command:


```
Quarks.install("/path/to/checked/out/SATIE");
```

> **Note:** SATIE depends on another quark, called _SC-HOA_, for its Higher-order Ambisonics capabilities. This quark will be installed automatically when installing SATIE.

You must restart the SuperCollider interpreter, or recompile the class library, in order to make SATIE available for use after installation.
