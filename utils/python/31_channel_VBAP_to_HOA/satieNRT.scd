// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


var order, path, exportFileName, info, speakerPos, config, satie, score, server;

if(thisProcess.argv.size == 2) {
    path = thisProcess.argv[0].asString;
    order = thisProcess.argv[1].asInteger;
} {
    1.exit;
    Error("Incorrect number of arguments. Expected a [path] and an ambisonic order [1-5].").throw
};

path = PathName(path);
info = SoundFile(path.files[0].fullPath).info;

if(info.numChannels != 1) {
    1.exit;
    Error("Sound files contain % channels. Expected monophonic sound files.".format(info.numChannels)).throw
};

if(path.files.size != 31) {
    1.exit;
    Error("Incorrect number of sound files found. Expected 31 files.").throw
};

try {
    // throws error if unable to create directory
    File.mkdir(path.fullPath +/+ "export")
} { |error|
    1.exit;
    Error(error).throw
};

// prevent overwriting any previous exports
exportFileName = path.fullPath +/+ "export" +/+ "export_ambiOrder_" ++ order ++ ".wav";
if(File.exists(exportFileName)) {
    1.exit;
    Error("exported audio file already exists. aborting.").throw
};

speakerPos = VBAPSpeakerArray(
    dim: 3,
    directions: [
        [0, 90], [-7.5, 55], [52.5, 55], [112.5, 55], [172.5, 55], [-127.5, 55], [-67.5, 55],
        [7.5, 20], [37.5, 20], [67.5, 20], [97.5, 20], [127.5, 20], [157.5, 20], [-172.5, 20],
        [-142.5, 20], [-112.5, 20], [-82.5, 20], [-52.5, 20], [-22.5, 20], [7.5, -15], [37.5, -15],
        [67.5, -15], [97.5, -15], [127.5, -15], [157.5, -15], [-172.5, -15], [-142.5, -15],
        [-112.5, -15], [-82.5, -15], [-52.5, -15], [-22.5, -15]
    ]
);

server = Server(\satieNRT);
config = SatieConfiguration(
    server: server,
    listeningFormat: [],
    ambiOrders: [order],
    minOutputBusChannels: (order+1).pow(2).asInteger
);
satie = Satie(config);
// we will manually create our SynthDefs, turning this off helps speed up booting
satie.config.generateSynthdefs = false;

fork {
    var buffers;
    var bundle;
    var timeout;
    var cond = Condition.new;
    var sourceName = ("sndFileAmbi" ++ order).asSymbol;
    var postProcName = ("ambipost__s0_o" ++ order).asSymbol;

    // start 10 sec timeout
    timeout = fork {
        10.wait;
        1.exit;
        Error("SATIE timed out while booting").throw
    };

    satie.waitForBoot { cond.unhang };
    cond.hang;

    // the /n_go message will be received when the following ambiPostProcessor is created in group \ambiPostProc
    OSCFunc({ cond.unhang }, '/n_go', argTemplate: [nil, satie.groups[\ambiPostProc].nodeID, nil, nil, 0]).oneShot;
    satie.makeAmbi(sourceName, \sndFile, ambiOrder: order, ambiBus: satie.config.ambiBus[0]);
    satie.replaceAmbiPostProcessor([\Identity], order: order, outputIndex: satie.config.outBusIndex);
    cond.hang;

    // stop 10 sec timeout
    timeout.stop;

    // in order to use Satie's generated SynthDefs in NRT, we need to store them
    satie.synthDescLib.synthDescs[sourceName].def.store;
    satie.synthDescLib.synthDescs[postProcName].def.store;

    // Satie is no longer needed at this point
    // we have a configured Server and stored SynthDefs to work with
    satie.quit;

    bundle = server.makeBundle(
        false,
        {
            buffers = path.files.size.collect { |index|
                Buffer.cueSoundFile(server, path.files[index].fullPath, 0, 1);
            };
            path.files.size.do { |index|
                Synth(
                    defName: sourceName,
                    target: server,
                    addAction: \addToHead,
                    args: [
                        \bufnum, buffers[index].bufnum,
                        \gainDB: -6,
                        \aziDeg: speakerPos.speakers[index].azi,
                        \eleDeg: speakerPos.speakers[index].ele
                    ]
                )
            };
            Synth(
                defName: postProcName,
                target: server,
                addAction: \addToTail
            );
        }
    );

    // To run this script using supernova instead of scsynth, run the following line
    // NOTE: using supernova in NRT with SATIE should be considered experimental
    // Score.program = "exec supernova";
    score = Score.new;
    bundle.do { |cmd| score.add([0.0, cmd]) };

    score.recordNRT(
        outputFilePath: exportFileName,
        sampleRate: info.sampleRate,
        headerFormat: "W64",
        sampleFormat: info.sampleFormat,
        options: server.options,
        duration: info.duration,
        action: {
            buffers.do { |buf| buf.free };
            server.remove;
            "finished rendering score".postln;
            "quitting sclang".postln;
            0.exit
        }
    )
}
