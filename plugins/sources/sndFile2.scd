// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin

*/


~name = \sndFile2;
~description = "A sound file player (stream from disk)";
~channelLayout = \mono;

~function = { |bufnum=0, loop=0, xfade=(-1)|
	var sig = DiskIn.ar(2, bufnum, loop: loop);
	XFade2.ar(sig[0], sig[1], xfade);
};

/*
~name = \sndFile;
~function = { | sfile = '/usr/share/sounds/alsa/Front_Center.wav' |
	var buf;
	var sf = sfile.asString;
	buf = Buffer.cueSoundFile(s, sf, 0, 1);
	DiskIn.ar(1, buf.bufnum);
};

*/
