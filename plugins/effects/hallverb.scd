// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin

*/

~name = \hallverb;
~description = "A mono reverb effect based on the NHHall UGen";
~channelLayout = \mono;

~function = { |in, outputDB=0, rt60=1, lowFq=200, lowRatio=0.5, hiFq=4000, hiRatio=0.5, earlyDiffusion=0.5, lateDiffusion=0.5, modRate=0.3, modDepth=0.3|

    // make a mono-to-mono reverb out of NHHall
    // NHHall takes a stereo input so we duplicate our mono input
    // then we turn the stereo setting up to 1
    // and only use the first output channel
    var input = In.ar(in);
    var sig = NHHall.ar(
        in: Pan2.ar(input),
        rt60: rt60,
        stereo: 1,
        lowFreq: lowFq,
        lowRatio: lowRatio,
        hiFreq: hiFq,
        hiRatio: hiRatio,
        earlyDiffusion: earlyDiffusion,
        lateDiffusion: lateDiffusion,
        modRate: modRate,
        modDepth: modDepth
    );
    sig[0] * outputDB.dbamp;
};
