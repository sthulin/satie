+ SatieOSC {
	getServerOption {
		^{ | args, time, addr, recvPort |
			if (args.size == 2)
			{
				var opts = format("%.%", satie.config.server.options.asCompileString, args[1]).interpret;
				returnAddress.sendMsg("/satie/server/option", args[1], opts);
			}
			{
				if(satie.debug, {
					"% wrong number of arguments. Should be /satie/server/options/get <optionName>"
					.format(this.class.getBackTrace).postln;});
			}
		};
	}
}