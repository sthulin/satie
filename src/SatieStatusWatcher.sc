SatieStatusWatcher {

	var satie;
	var server;
	var <>status;
	var <booted;

	*new { |satie|
		^super.newCopyArgs(satie).init
	}

	init {
		status = \initialized;
		booted = false; // for backwards compatibility
	}

	satieBoot {
		if(status !== \initialized) {
			Error("% Satie unable to boot. Status was \'%\', should have been \'initialized\'".format(thisMethod, status)).throw;
		};

		server = satie.config.server;
		status = \booting;
		this.watchServerBoot;
	}

	watchServerBoot {
		// boot the server
		server.waitForBoot(this.postServerBoot);

		Routine {
			// wait for serverBooting to become true if it isn't already
			var limit = 10;
			while { (server.serverBooting.not) && (limit > 0) } {
				0.05.wait;
				limit = limit - 1;
			};
			// if false, skip to the next step
			// if true, loop until false after booting is done
			while { server.serverBooting } {
				0.05.wait;
			};
			// if serverRunning is false, boot was unsuccessful
			if(server.serverRunning.not) {
				satie.init;
				Error("% Satie was unable to boot. Server boot failed.".format(thisMethod)).throw;
			};
		}.play(AppClock);
	}

	waitForSatieRunning { |onComplete|
		switch(status)
			{\initialized} {
				this.satieBoot;
				this.doWhenSatieRunning(onComplete);
			}
			{\booting} {
				this.doWhenSatieRunning(onComplete);
			}
			{\running} {
				this.doWhenSatieRunning(onComplete);
			}
			{
				Error("% Cannot boot Satie while status is \'%\'".format(thisMethod, status)).throw;
			};
	}

	doWhenSatieRunning { |onComplete|
		if((status !== \booting) && (status !== \running)) {
			Error("% invalid status \'%\'".format(thisMethod, status)).throw;
		};

		Routine {
			var limit = 50;
			while { (status === \booting) && (limit > 0) } {
				limit = limit - 1;
				0.2.wait;
			};
			if(status === \running) {
				// synching before onComplete allows us to call sync inside onComplete without issue
				server.sync;
				onComplete.value;
			} {
				Error("% Satie failed to boot. Status was \'%\', should have been \'running\'".format(thisMethod, status)).throw;
			};
		}.play(AppClock);
	}

	postServerBoot {
		// returns a function so that it can be evaluated by Server.waitForBoot
		^{
			ServerTree.add(satie, server);
			ServerQuit.add(this, server);
			CmdPeriod.add(satie);

			satie.postBootExec;

			status = \running;
			booted = true; // for backwards compatibility
		}
	}

	satieReboot {
		switch(status)
			{\initialized} { this.satieBoot; }
			{\running} {
				this.satieQuit(
					onComplete: { |satie|
						"Satie rebooting...".postln;
						satie.boot;
					}
				);
			}
			{
				Error(
					"% Satie failed to reboot. Status was %, expected \'initialized\' or \'running\'"
						.format(thisMethod, status)
				).throw;
			}
	}

	satieQuit { |quitServer = true, onComplete|
		if(status !== \running) {
			Error("% Satie unable to quit. Status is not \'running\''".format(thisMethod, status)).throw;
		};

		status = \quitting;
		this.preQuit;

		if(quitServer && server.serverRunning) {
			this.watchServerQuit(onComplete);
		} {
			this.init;
		};
	}

	watchServerQuit { |onComplete|
		var timeout, responder;

		responder = OSCFunc({ |msg|
			if(msg[1] === '/quit') {
				timeout.stop;
				responder.free;

				this.init;

				// onComplete can be called safely even when it is nil
				AppClock.sched(
					delta: 1,
					item: {
						onComplete.value(satie);
						nil; // return nil to avoid rescheduling function
					}
				);
			}
		}, '/done', server.addr);

		timeout = Routine {
			// 3 secs matches SC Server's quit failure timeout
			3.wait;
			"% Satie timed out while quitting its Server".format(thisMethod).error;
			responder.free;
		}.play(AppClock);

		// quit Server
		server.quit;
	}

	preQuit {
		CmdPeriod.remove(satie);
		ServerTree.remove(satie, server);
		ServerQuit.remove(this, server);
		satie.cleanSlate;
		satie.clearBuffers;
		satie.clearOSC;

		satie.init;
	}

	doOnServerQuit {
		"SATIE - server is quitting".postln;
		this.preQuit;
	}

}

