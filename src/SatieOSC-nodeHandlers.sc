+ SatieOSC {

	// expects /oscaddress nodeName pluginName groupName<optional> arg1 value1 argN valueN...<optional>
	createSourceHandler {
		^{ |args|
			var sourceName, synthName, groupName, synthArgs;

			if(satie.debug) { "→    %: message: %".format(this.class.getBackTrace, args).postln };

			if(args.size < 3) {
				"→    %: message: bad arg count: expects  at least 2 values:  nodeName, pluginName, and optionally:  groupName, arg1, value1, argN, valueN".format(this.class.getBackTrace).error
			} {
				sourceName = args[1].asSymbol;
				synthName  = args[2].asSymbol;
				groupName = \default;

				if(args.size > 3) {
					groupName = args[3].asSymbol;
					synthArgs = args[4..]; // returns an empty array if no values exist at these indexes
					this.createSourceNode(sourceName, synthName, groupName, synthArgs);
				} {
					this.createSourceNode(sourceName, synthName, groupName);
				}
			}
		}
	}

	// expects /oscaddress pluginName groupName<optional> arg1 value1 argN valueN...<optional>
	createKamikazeHandler {
		^{ |args|
			var synthName, groupName, synthArgs;

			if(satie.config.debug) { "→    %: message: %".format(this.class.getBackTrace, args).postln };

			if(args.size < 2) {
				"→    %: message: bad arg count: expects  at least 1 value:  pluginName, and optionally:  groupName, arg1, value1, argN, valueN".format(this.class.getBackTrace).error
			} {
				synthName  = args[1].asSymbol;
				groupName = \default;

				if(args.size > 2) {
					groupName = args[2].asSymbol;
					synthArgs = args[3..]; // returns an empty array if no values exist at these indexes
					this.createKamikazeNode(synthName, groupName, synthArgs);
				} {
					this.createKamikazeNode(synthName, groupName);
				}
			}
		}
	}


	// expects /oscaddress nodeName pluginName groupName<optional> auxBus<optional> arg1 value1 argN valueN...<optional>
	createEffectHandler {
		^{ |args|
			var sourceName, synthName, groupName, auxBusIndex, synthArgs;

			if(satie.debug) { "→    %: message: %".format(this.class.getBackTrace, args).postln };

			if(args.size < 3) {
				"→    %: message: bad arg count: expects  at least 2 values:  nodeName, pluginUri, and optionally:  groupName".format(this.class.getBackTrace).error
			} {
				sourceName = args[1];
				synthName  = args[2];
				groupName = \defaultFx;
				auxBusIndex = 0;
				synthArgs = #[];

				if(args.size > 3) {
					groupName = args[3];
					auxBusIndex = args[4] ?? { 0 }; // default to 0 if no value exist at that index
					synthArgs = args[5..]; // returns an empty array if no values exist at these indexes
				};

				this.createEffectNode(sourceName, synthName, groupName, auxBusIndex, synthArgs);
			}
		}
	}

	// expects /oscaddress id processName <optional>arg1 value1 ... argN valueN
	createProcessHandler {
		^{ |args|
			var id, processName, arglist;

			if(satie.debug) {"→    %: message: %".format(this.class.getBackTrace, args).postln};

			if(args.size < 3) {
				"→    %: message: bad arg count: expects  at least 2 values:  nodeName, pluginName, and optionally: groupName".format(this.class.getBackTrace).error
			} {
				id = args[1].asSymbol;
				processName = args[2].asSymbol;
				arglist = args[3..]; // returns an empty array if there are no values at these indexes
				this.createProcessNode(id, processName, arglist);
			}
		}
	}


	// expects /oscaddress groupName
	createSourceGroupHandler {
		^{ | args |

			if (satie.debug, {"→    %: message: %".format(this.class.getBackTrace, args).postln});

			if ( (args.size < 2 ) ,
				{
					"→    %: message: missing  nodeName argument".format(this.class.getBackTrace).error
				},
				// else
				{
					var groupName = args[1].asSymbol;
					this.createGroup(groupName, \addToHead);
			});
		}
	}

	// expects /oscaddress groupName
	createEffectGroupHandler {
		^{ | args |

			if (satie.debug, {"→    %: message: %".format(this.class.getBackTrace, args).postln});

			if ( (args.size < 2 ) ,
				{
					"→    %: message: missing  nodeName argument".format(this.class.getBackTrace).error
				},
				// else
				{
					var groupName = args[1].asSymbol;

					this.createGroup(groupName, \effect);
			});
		}
	}

	// expects /oscaddress groupName
	createProcessGroupHandler {
		^{ | args |

			if (satie.debug, {"→    %: message: %".format(this.class.getBackTrace, args).postln});

			if ( (args.size < 2 ) ,
				{
					"→    %: message: missing  nodeName argument".format(this.class.getBackTrace).error
				},
				// else
				{
					var groupName = args[1].asSymbol;

					this.createGroup(groupName, \addToHead);
			});
		}
	}


	deleteNodeHandler {
		^{ |args|
			if(satie.debug, {"→    %: message: %".format(this.class.getBackTrace, args).postln});

			if(args.size != 2, {
				"→    %: message missing nodeName %".format(this.class.getBackTrace, args).error
			}, {
				var nodeName = args[1].asSymbol;

				// first check if node is a process as it is a special case
				if(satie.processInstances.includesKey(nodeName), {
					satie.cleanProcessInstance(nodeName)
				}, {
					if(satie.groupInstances.includesKey(nodeName), {
						this.removeGroup(nodeName)
					}, {
						this.deleteSource(nodeName)
					})
				})
			})
		}
	}

	clearSceneHandler {
		^{ | args |

			if (satie.debug, {"→    %: message: %".format(this.class.getBackTrace, args).postln});

			satie.clearScene;
		}
	}

	debugFlagHandler {
		^{ | args |
			if ((args.size !=2 ),
				{"% message missing flag".format(this.class.getBackTrace).warn},
				{
					"→    %:  message: %".format(this.class.getBackTrace, args).postln;
					satie.debug = args[1].asInteger.asBoolean;
				}
			)
		}
	}

	update_message_size{
		var ret;
		^(update_message_keys.size() + update_custom_keys.size() + 2);
	}

	updateSrcHandler {
		^{ |args|
			var nodeName = args[1];
			var thisSynth = this.getSourceNode(nodeName, \synth);
			var msg_size = this.update_message_size();

			if(args.size  == msg_size, {
				if(satie.debug, {
					"→    %: message: %".format(this.class.getBackTrace, args).postln
				});
				this.updateNode(thisSynth, args);
			}, {
				"→    Received a message of size is %, but expected: %.".format(args.size, msg_size).warn;
				this.updateNode(thisSynth, args);
			})
		}
	}

	updateGroupHandler {
		^{ |args|
			var nodeName = args[1];
			var thisGroup = this.getGroupNode(nodeName, \group);
			var msg_size = this.update_message_size();

			if(args.size  == msg_size, {
				this.updateNode(thisGroup, args);
			}, {
				"→    Received a message of size is %, but expected: %.".format(args.size, msg_size).warn;
				this.updateNode(thisGroup, args);
			})
		}
	}

	updateProcHandler {
		^{ | args |
			var nodeName = args[1].asSymbol;
			var thisGroupName, thisGroup, myProcess;
			var msg_size = this.update_message_size();

			if (args.size != msg_size,
				{"→    Received a message of size is %, but expected: %.".format(args.size, msg_size).warn;},
				{
					if (satie.debug,
						{
							"%: args: %".format(this.class.getBackTrace, args).postln;
						}
					);
					thisGroupName = (nodeName++"_group").asSymbol;
					thisGroup = satie.groups[thisGroupName.asSymbol];
					myProcess = satie.processInstances[nodeName];
					if (myProcess.isNil, {
						"%: process node %: BUG FOUND: undefined process"
						.format(this.class.getBackTrace, nodeName).error}
					);
					if (myProcess[\setUpdate].isNil,
						{
							if (satie.debug,
								{
									"%: no setUpdate on % so we update group % with args %".
									format(this.class.getBackTrace, myProcess, thisGroup, args).postln;
								}
							);
							this.updateNode(thisGroup, args);
						},
						{
							var aziDeg, eleDeg, gainDB, delayMs, lpHz;
							aziDeg = args[2] + satie.config.orientationOffsetDeg[0];
							eleDeg= args[3] + satie.config.orientationOffsetDeg[1];
							gainDB = args[4];
							delayMs = args[5];
							lpHz = args[6];
							myProcess[\setUpdate].value(myProcess, aziDeg, eleDeg, gainDB, delayMs, lpHz, args[7..]);
						}
					);
				}
			);
		};
	}


	// for sources and groups
	updateNode { | node, args |
		var pairs;
		var final_keys = [];

		if (satie.debug, {"→    %: message: %".format(this.class.getBackTrace, args).postln});

		args[2] = args[2] + satie.config.orientationOffsetDeg[0];
		args[3] = args[3] + satie.config.orientationOffsetDeg[1];

		final_keys = update_message_keys ++ update_custom_keys;
		// interleave the two inner arrays
		// will be as long as shortest array, thus dropping 'distance' when absent
		pairs = [
			final_keys,
			args[2..]
		].lace;
		node.set(*pairs);
	}

	setProcHandler {
		^{ |args|
			var nodeName = args[1].asSymbol;
			var props = args[2..];

			if(args.size < 4) {
				"%: invalid OSC message. Expectinig a minimum of 4 values. OSC message was: %".format(thisMethod, args).error;
			} {
				if(satie.debug) {
					"%: setting process: %, properties: %".format(thisMethod, nodeName, props).postln;
				};

				if(satie.processInstances.includesKey(nodeName).not) {
					"%: process: % is undefined".format(thisMethod, nodeName).error;
				} {
					var processGroup = satie.groups.at((nodeName ++ "_group").asSymbol);
					var process = satie.processInstances[nodeName];

					this.processSet(process, processGroup, props);
				}
			}
		}
	}

	setGroupHandler {
		^{ | args |
			var nodeName = args[1];
			var props = args.copyRange(2, args.size -1);
			var targetNode;

			if (satie.debug, {"→    %: message: %".format(this.class.getBackTrace, args).postln});
			if (satie.groups.includesKey(nodeName.asSymbol),
				{
					targetNode = this.getGroupNode(nodeName, \group);
					this.nodeSet(targetNode, props);
				},
				{
					"→   %: group node % does not exist".format(this.class.getBackTrace, nodeName).error;
				}
			);
		}
	}

	setSrcHandler {
		^{| args |
			var nodeName = args[1];
			var props = args.copyRange(2, args.size -1);
			var targetNode;

			if (satie.debug, {"→ %: message: %".format(this.class.getBackTrace, args).postln});
			targetNode = this.getSourceNode(nodeName);
			// this.nodeSet(targetNode, props);
			if (targetNode.isNil,
				{
					error("%: source node: % - bug: undefined synth: %".format(this.class.getBackTrace, targetNode, nodeName));
				},
				{
					this.nodeSet(targetNode, props);
				}
			);
		}
	}

	nodeSet {| targetNode, props |

		if (satie.debug, {"→ %:\n    → targetNode: %\n     →properties: % ".format(this.class.getBackTrace, targetNode, props).postln});

		props.pairsDo({|prop, val|
			switch(prop,
				'hpHz',
				{
					targetNode.set(\hpHz , clip(val, 5, 20000));
				},
				'spread',
				{
					targetNode.set(\spread, val.clip(0, 1));
				},
				'in',
				{
					targetNode.set(prop, satie.aux[val.asInteger]);
				},
				{
					targetNode.set(prop, val);
				}
			)
		})
	}

	processSet { |process, group, props|

		if(satie.debug) {
			"%: setting process: %, properties: %".format(this.class.getBackTrace, process.name, props).postln;
		};

		if(props.size.odd) {
			"%: process: %, odd number of arguments provided: %".format(thisMethod, process.name, props).error;
		} {
			var hasFunc = process[\set].class === Function;

			props.pairsDo { |key, value|
				// skip this key-value if key is a function
				if(process[key].class === Function) {
					"%: cannot set process: %, property: % is a function".format(thisMethod, process.name, key).warn;
				} {
					// prioritise setting as follows:
					// 1. use set function
					// 2. set environment key
					// 3. set group
					if(hasFunc) {
						process.perform(\set, key, value);
					} {
						if(process.includesKey(key)) {
							process.put(key, value);
						} {
							group.set(key, value);
						}
					}
				}
			}
		}
	}

	propertyProcHandler {
		^{ |args|
			var nodeName = args[1].asSymbol;
			var props = args[2..];

			if(args.size < 4) {
				"%: invalid OSC message. Expectinig a minimum of 4 values. OSC message was: %".format(thisMethod, args).error;
			} {
				if(satie.debug) {
					"%: setting process: %, properties: %".format(this.class.getBackTrace, nodeName, props).postln;
				};

				if(satie.processInstances.includesKey(nodeName).not) {
					"%: process % is undefined".format(thisMethod, nodeName).error;
				} {
					var process = satie.processInstances[nodeName];
					var group = satie.groups.at((nodeName++"_group").asSymbol);
					var hasFunc = process[\property].class == Function;

					if(props.size.odd) {
						"%: process: %, odd number of arguments provided: %".format(thisMethod, process.name, props).error;
					} {

						if(hasFunc.not) {
								"%: cannot call property on process: %, no property function found. Did you mean to call /satie/process/set?".format(
									this.class.getBackTrace, process.name).error;
						} {
							props.pairsDo { |key, value|
								// skip this key-value if key is a function
								if(process[key].class === Function) {
									"%: cannot set process: %, property: % is a function".format(thisMethod, process.name, key).warn;
								} {
									process.perform(\property, key, value);
								}
							}
						}
					}
				}
			}
		}
	}

	evalFnProcHandler {
		^{ | args |

			if (satie.debug, { postf("SatieOSC.evalProcFnHandler:  mess: %\n", args); });

			// verify data
			if (  ( args.size < 3)  ,
				{
					error("SatieOSC.evalProcFnHandler: bad message length: expects oscAddress key data1 or more \n"++args);
				}, // else args good
				{
					var nodeName  = args[1].asSymbol;
					var key = args[2];
					var vector = args.copyRange(3, args.size - 1); // nil is OK.
					var targetNode = nil;

					var myProcess = satie.processInstances.at(nodeName);

					if ( myProcess.isNil,
						{
							"%: process node: % does not exist".format(this.class.getBackTrace, myProcess).error;
						},
						{  // good to go
							if ( myProcess[key.asSymbol].class == Function,  // does a specific handler exist for this key ?
								{
									myProcess[key.asSymbol].value(myProcess, vector);   // use process's specially defined message handler for this key
								},
								{
									// else  nope:  no handler with that name exists.  Check  if a handler named \setVec is defined.
									warn("SatieOSC.evalProcFnHandler:  process node: "++nodeName++"  undefined method: "++key.asSymbol++"  , can not service message \n");
								});
						});
				});
		}
	}

	setVecProcHandler {
		^{ | args |

			if (satie.debug, { postf("SatieOSC.setVecProcHandler:  mess: %\n", args); });

			if (  ( args.size < 3)  ,   			// verify data
				{
					error("SatieOSC.setVecHandler: bad message length: expects oscAddress key data1 or more \n"++args);
				}, // else args good
				{
					var nodeName  = args[1].asSymbol;
					var key = args[2];
					var vector = args.copyRange(3, args.size - 1); // nil is OK.
					var targetNode = nil;

					if ( satie.processInstances.includesKey(nodeName) == true,
						{
							var thisGroup = satie.groups.at((nodeName++"_group").asSymbol);
							var myProcess = satie.processInstances.at(nodeName);
							var matched = false;

							if ( myProcess.isNil,
								{
									error("SatieOSC.setVecHandler:  process node: "++nodeName++"  BUG FOUND: undefined process  \n");

								},
								{  // good to go
									if ( myProcess[key.asSymbol].class == Function,  // does a specific handler exist for this key ?
										{
											matched = true;
											myProcess[key.asSymbol].value(myProcess, vector);   // use process's specially defined message handler for this key
										},
										{
											// else  nope:  no handler with that name exists.  Check  if a handler named \setVec is defined.
											if ( myProcess[\setVec].class == Function,
												{
													matched = true;
													myProcess[\setVec].value(myProcess, key, vector);   // use process's \setVec message handler
											});
									});
									//
									if (matched == false,
										{
											// or just update the process's group
											thisGroup.set(key,vector);
									});
							});
						},
						{  // else error
							error("SatieOSC.setVecHandler:  process node: "++nodeName++"  is undefined \n");
					});
			});
		}
	}

	setVecSourceHandler {
		^{ | args |

			if (satie.debug, { postf("satieOSC.setVecSourceHandler:  mess: %\n", args); });

			if (  ( args.size < 3)  ,   // verify data
				{
					error("SatieOSC.setVecHandler: bad message length: expects oscAddress key data1 or more \n"++args);
				}, // else args good
				{
					var nodeName  = args[1];
					var key = args[2];
					var vector = args.copyRange(3, args.size - 1);
					var targetNode;

					targetNode = this.getSourceNode(nodeName);
					targetNode.set(key, vector);
			});
		}
	}

	setVecGroupHandler {
		^{ | args |

			if (satie.debug, { postf("SatieOSC.setVecGroupHandler:  mess: %\n", args); });

			if (  ( args.size < 3)  ,   // verify data
				{
					error("SatieOSC.setVecHandler: bad message length: expects oscAddress key data1 or more \n"++args);
				}, // else args good
				{
					var nodeName  = args[1];
					var key = args[2];
					var vector = args.copyRange(3, args.size - 1);
					var targetNode;

					targetNode = this.getGroupNode(nodeName, \group);
					targetNode.set(key, vector);
			});
		}
	}

	// handles /satie/source/state  nodeName flag
	stateSourceHandler {
		^{ | args |

			if ( satie.debug,
				{
					postf("SatieOSC.stateSourceHandler: % \n", args);
			});

			// verify message
			if (  ( args.size != 3)  ,
				{
					error("SatieOSC.stateSourceHandler: bad message length: expects oscAddress nodeName val % \n", args);
				}, // else args good
				{
					var nodeName  = args[1];
					var value = args[2];
					var targetNode = nil;
					var state;

					if ( value == 0 , { state = false}, {state = true});

					targetNode = this.getSourceNode(nodeName);
					if ( targetNode.isNil,
						{
							error("SatieOSC.stateSourceHandler:  source node: "++nodeName++"  BUG FOUND: undefined SYNTH  \n");
						}, // else good to go
						{
							targetNode.run(state);
						});
				});
		}
	}

	// handles /satie/group/state  nodeName flag
	stateGroupHandler {
		^{ | args |

			if ( satie.debug,
				{
					postf("SatieOSC.stateGroupHandler: % \n", args);
			});

			// verify message
			if (  ( args.size != 3)  ,
				{
					error("SatieOSC.stateGroupHandler: bad message length: expects oscAddress nodeName val % \n", args);
				}, // else args good
				{
					var nodeName  = args[1];
					var value = args[2];
					var targetNode = nil;
					var state;

					if ( value == 0 , { state = false}, {state = true});

					if (  satie.groups.includesKey (nodeName.asSymbol) == true,
						{
							targetNode = satie.groups[nodeName.asSymbol];
							targetNode.run(state);

						},
						{   // else no group
							error("SatieOSC.stateGroupHandler:  group node: "++nodeName++"  is undefined \n");
					});
			});
		}
	}

	// handles /satie/process/state  nodeName flag
	stateProcHandler {
		^{ | args |

			if ( satie.debug,
				{
					postf("SatieOSC.stateProcHandler: % \n", args);
				});

			// verify message
			if (  ( args.size != 3)  ,
				{
					error("SatieOSC.stateProcHandler: bad message length: expects oscAddress nodeName val % \n", args);
				}, // else args good
				{
					var processName  = args[1].asSymbol;
					var value = args[2];
					var thisGroup;
					var myProcess = satie.processInstances[processName];
					var state;

					if ( value == 0 , { state = false}, {state = true});

					thisGroup = (processName++"_group").asSymbol;

					if (myProcess.isNil,
						{
							"%: undefined process: %".format(this.class, processName).warm;
						},
						{
							if (myProcess[\state].class == Function,
								{
									myProcess[\state].value(myProcess, state);
								},
								{
									if ( thisGroup.notNil,
										{
											satie.groups[thisGroup.asSymbol].run(state);
										},
										{ "%: process % missing state method and no group is defined".format(this.class, processName).error;}
									);
								}
							);
						});
				});
		};
	}

}  // end of context
